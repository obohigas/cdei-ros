#ifndef DIFF_DRIVE_H
#define DIFF_DRIVE_H

#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <cdei_ros/WheelSpeed.h>

namespace diff_drive
{

class DiffDrive
{

	protected:

		ros::NodeHandle nh__;
		ros::Subscriber twist_subscriber__;
		ros::Publisher wheel_speed_publisher__;
		cdei_ros::WheelSpeed wheel_speeds__;
		ros::Timer timer__;
		double wheel_distance__;

	public:

		DiffDrive();
		~DiffDrive();

		bool init();
		bool close();

	protected:

		void twistCallback(const geometry_msgs::Twist & __twist);
		void timerCallback(const ros::TimerEvent & __timer_event);

};

}

#endif
