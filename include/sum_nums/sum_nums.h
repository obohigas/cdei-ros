#ifndef SUM_NUMS_H
#define SUM_NUMS_H

#include <ros/ros.h>
#include <cdei_ros/SumNums.h>

namespace sum_nums
{

class SumNums
{

	protected:

		ros::NodeHandle nh__;

		ros::ServiceServer sum_server__;

	public:

		SumNums();
		~SumNums();

		bool init();
		bool close();

	protected:

		bool sumNumsServerCallback(cdei_ros::SumNums::Request & __request, cdei_ros::SumNums::Response & __response);
};

}

#endif
