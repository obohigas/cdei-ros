#include "diff_drive/diff_drive.h"

namespace diff_drive
{

DiffDrive::DiffDrive():nh__(){}
DiffDrive::~DiffDrive(){}

bool DiffDrive::init()
{
	if ( !nh__.getParam("wheel_distance", wheel_distance__) )
	{
		ROS_ERROR("Failed to get wheel_distance parameter");
		return false;
	}
	twist_subscriber__ = nh__.subscribe("twist", 1, &DiffDrive::twistCallback, this);
	wheel_speed_publisher__ = nh__.advertise<cdei_ros::WheelSpeed>("wheel_speeds", 1, true);
	timer__ = nh__.createTimer(ros::Rate(5.0), &DiffDrive::timerCallback, this, false, false);
	timer__.start();
	return true;
}

bool DiffDrive::close()
{
	return true;
}

void DiffDrive::twistCallback(const geometry_msgs::Twist & __twist)
{
	// IMPLEMENT KINEMATICS HERE!
	wheel_speeds__.right = __twist.linear.x + __twist.angular.z*wheel_distance__/2.0;
	wheel_speeds__.left = __twist.linear.x - __twist.angular.z*wheel_distance__/2.0;
}

void DiffDrive::timerCallback(const ros::TimerEvent & __timer_event)
{
	wheel_speed_publisher__.publish(wheel_speeds__);
}

}
