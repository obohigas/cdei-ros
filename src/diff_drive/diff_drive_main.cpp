#include "diff_drive/diff_drive.h"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "diff_drive");

	diff_drive::DiffDrive diff_drive;

	if ( !diff_drive.init() )
	{
		ROS_ERROR("Failed to init");
		return -1;
	}

	ros::spin();

	if ( !diff_drive.close() )
	{
		ROS_ERROR("Failed to close");
		return -1;
	}

	return 0;
}
