#include "sum_nums/sum_nums.h"

namespace sum_nums
{

SumNums::SumNums():nh__(){}
SumNums::~SumNums(){}

bool SumNums::init()
{
	sum_server__ = nh__.advertiseService("sum_nums", &SumNums::sumNumsServerCallback, this);
	return true;
}

bool SumNums::close()
{
	return true;
}

bool SumNums::sumNumsServerCallback(cdei_ros::SumNums::Request & __request, cdei_ros::SumNums::Response & __response)
{
	__response.result = __request.a + __request.b;
	return true;
}
}
