#include "sum_nums/sum_nums.h"

int main(int argc, char **argv)
{
	ros::init(argc, argv, "sum_nums");

	sum_nums::SumNums sum_nums;

	if ( !sum_nums.init() )
	{
		ROS_ERROR("Failed to init");
		return -1;
	}

	ros::spin();

	if ( !sum_nums.close() )
	{
		ROS_ERROR("Failed to close");
		return -1;
	}

	return 0;
}
